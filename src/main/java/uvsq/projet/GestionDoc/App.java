package uvsq.projet.GestionDoc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import projet.command.Commande;



/**
 * Hello world!
 *
 */
public class App 
{
 
	
	public static void main(String[] args) {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Welcome :) ! \n ");
		System.out.println("    Add Folder : pour ajouter un repertoire  \n"
				+"-------------------------------------------------------- \n"
				+ "    Add Text File :  pour l'ajout d'un fichier texte   \n "
				+"-------------------------------------------------------- \n"
				+ "    Add Pdf File :  pour l'ajout d'un fichier PDF  \n "
				+"-------------------------------------------------------- \n"
				+ "    Create Folder  : Pour creer un repertoire"
				+"-------------------------------------------------------- \n"
				+ "    Delete Folder  :  Pour supprimer un repertoire \n "
				+"-------------------------------------------------------- \n"
		        + "    Delete Text File  : pour supprimer un fichier text stocké dans l'INBOX \n "
		        +"-------------------------------------------------------- \n"
		        + "    Delete Pdf File  : pour supprimer un fichier Pdf stocké dans l'INBOX \n");
		        
		while(true){
			String s;
			try {
				s = br.readLine();
			Commande c = Read.nextCommand(s);
				if(c == null)
					System.out.println("la commande que vous avez entré n'est pas valide");
				else c.execute(); 
			} catch (IOException e) {
				e.printStackTrace();
			}
		}	
		
	}
		
		
		
		
	
	
	
}
