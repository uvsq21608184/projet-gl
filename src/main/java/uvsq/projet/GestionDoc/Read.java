package uvsq.projet.GestionDoc;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

import projet.command.*;;



public class Read {

	public Read() {
		
	}

	public static  Commande nextCommand(String str){
		
		if(str.equals("Add Pdf File")){
			
			System.out.println("Donnez le chemin de votre fichier pdf");
	
			
			  Scanner sc = new Scanner(System.in);
		        String entree =  sc.nextLine();
			
			return new AddPdfFile(entree);
			
			
			
		}
	

		if(str.equals("Add Folder")){
			
			
			System.out.println("Donnez le nom de repertoire a creer");
			
			
			  Scanner sc = new Scanner(System.in);
		        String entree =  sc.nextLine();
		   return new CreateFolder(entree);
		    
		}
		if(str.equals("Add Text File")){
			
			
			System.out.println("Donnez le nom de fichier a ajouter");
			
			
			  Scanner sc = new Scanner(System.in);
		        String entree =  sc.nextLine();
			return new AddTextFile(entree);
		}
		if(str.equals("Delete Text File")){
			
			
			System.out.println("Donnez le nom du fichier a supprimer");
			
			
			  Scanner sc = new Scanner(System.in);
		        String entree =  sc.nextLine();
			return new DeleteFileText(entree);
		}
	if(str.equals("Delete Pdf File")){
			
			
			System.out.println("Donnez le nom du fichier a supprimer");
			
			
			  Scanner sc = new Scanner(System.in);
		        String entree =  sc.nextLine();
			return new DeleteFilePdf(entree);
		}
	
	if(str.equals("Delete Folder")){
		
		
		System.out.println("Donnez le nom du fichier a supprimer");
		
		
		  Scanner sc = new Scanner(System.in);
	        String entree =  sc.nextLine();
		return new DeleteFolder(entree);
	}
	
	if(str.equals("Create Folder")){
		
		
		System.out.println("Donnez le nom du fichier a creer");
		
		
		  Scanner sc = new Scanner(System.in);
	        String entree =  sc.nextLine();
		return new CreateFolder(entree);
	}
	
		
		
			
		
		return null;

}}
