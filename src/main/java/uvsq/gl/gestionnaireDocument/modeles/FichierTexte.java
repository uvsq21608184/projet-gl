package uvsq.gl.gestionnaireDocument.modeles;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.hibernate.annotations.Entity;


@javax.persistence.Entity
@DiscriminatorValue("FichierTexte")
public class FichierTexte extends Document  {



	

	private String contenue;
	
	public FichierTexte() {
		super("texte");
		contenue= "";
	}
	
	public void chargerContenue(){
		if(super.getChemin()!=null && !super.getChemin().equals("")){
			try{
				File f = new File(super.getChemin());
				BufferedReader br = new BufferedReader(new FileReader(f));
				String line="";
				while((line = br.readLine()) != null){
					contenue += (line +"\n"); 
				}
			
			}catch(Exception e){
				System.out.println("Impossible de charger le contenue du fichier !! ");
			}
			
		}
	
	}

	public String getContenue() {
		return contenue;
	}

	public void setContenue(String contenue) {
		this.contenue = contenue;
	}
	
	

}
