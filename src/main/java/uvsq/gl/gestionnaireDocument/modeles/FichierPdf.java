package uvsq.gl.gestionnaireDocument.modeles;



import java.io.File;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.asprise.ocr.Ocr;

@Entity
@DiscriminatorValue("PdfImage")
public class FichierPdf extends Document{
	
	private String contenue;
	
	public FichierPdf() {
		super("PDF");
		contenue="";
	}

	public void chargerContenue(){
		if(super.getChemin()!=null && !super.getChemin().equals("")){
			try{
			
				Ocr.setUp(); // lancer OCR
				Ocr ocr = new Ocr(); // creer une nouvelle machine ocr
				ocr.startEngine("eng", Ocr.SPEED_FASTEST); // English
				contenue = ocr.recognize(new File[] {new File(super.getChemin())}, Ocr.RECOGNIZE_TYPE_ALL, Ocr.OUTPUT_FORMAT_PLAINTEXT);
			//	System.out.println("Result: " + s);
				// ocr more images here ...
				ocr.stopEngine();
				
				
		
		
			}catch(Exception e){
				System.out.println("Impossible de charger le contenue du fichier !! ");
			}
		}
		
	}

	public String getContenue() {
		return contenue;
	}

	public void setContenue(String contenue) {
		this.contenue = contenue;
	}
	
	
}
