package uvsq.gl.gestionnaireDocument.modeles;


import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.OneToMany;



@Entity
@DiscriminatorValue("Repertoire")
public class Repertoire extends Document  {
	
	
	@Embedded
	private List<Document> documents;

	
	public Repertoire() {
		super("Repertoire");
		documents = new Vector<Document>();

	}
	
	/**
	 * Méthode permettant d'ajouter un document
	 * @param d
	 */
	public void ajouterDocument(Document d){
		documents.add(d);
	}
	
		
	/**
	 * Méthode permettant de lister l'hiéarchie du répertoire
	 * @return la chaine de caractère représentant l'hiéarchie du répertoire
	 */
	public String listerDocuments(){
	
		String s = "- "+super.getNom()+"\n";
		for(Iterator i1=documents.iterator();i1.hasNext();) {
			Document d =(Document) i1.next();
				if(d.getType().equals("Repertoire") ){
					 Repertoire r = (Repertoire) d;
					   s+=r.listerDocuments();
				}else {
					
					 s+= ("-> "+d.getNom()+"\n");
				}
		}
	
		return s;
	}
	
	/**
	 * 
	 * Méthode permettant de supprimer un document 
	 * @param nom du document
	 */
	public void supprimerDocument(String nom){
		for (Document document : documents) {
			if(document.getNom().equals(nom)){
				documents.remove(document);
			}
		}
	}

	/**
	 * 
	 * Methode permettant de creer un iterator pour les documents
	 */

	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	public Iterator<Document> iterator() {
		// TODO Auto-generated method stub
		return documents.iterator();
	}


	

}
