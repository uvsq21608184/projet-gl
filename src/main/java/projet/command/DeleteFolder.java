package projet.command;

import java.util.ArrayList;

import projet.Dao.DAOFactory;
import projet.Dao.RepertoireDAO;
import projet.Dao.RepertoireFichierDAO;
import projet.data.Repertoire;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

public class DeleteFolder implements Commande {

	
	private String nomRepertoire;
	private String nomRepertoireParent;
	private static final RepertoireDAO repDAO = DAOFactory.getRepertoireDAO();
	private static final RepertoireFichierDAO repFicDAO = DAOFactory.getRepertoireFichierDAO();
	 
	public DeleteFolder(String nom) {
		nomRepertoire = nom;
		String[] reps = Repertoire.getActuel().split("/");
		nomRepertoireParent = reps[reps.length-1];
	}
	
	public void execute() {
		delete(nomRepertoireParent, nomRepertoire);
	}
	
	public void delete(String nomRepertoireParent, String nomRepertoire) 
	{
		if (repDAO.existeFils(nomRepertoireParent,nomRepertoire)) {
		
			ArrayList<String> repsFils = repDAO.findFils(nomRepertoire);
			for (String repFils : repsFils)
				delete(nomRepertoire, repFils);

			ArrayList<String> fichiersFils = repFicDAO.findFichiersFils(nomRepertoire);
			for (String fichierFils : fichiersFils)
				repFicDAO.delete(nomRepertoire, fichierFils);
		
			repDAO.delete(nomRepertoire);
		}
		else
			System.err.println(ansi().fgBrightRed().a("\n----- Erreur ----- : le repertoire \""+nomRepertoire+"\" n'existe pas dans le répertoire courant.\n").reset());
	}
	
}
