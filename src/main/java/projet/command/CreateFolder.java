package projet.command;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import projet.Dao.DAOFactory;
import projet.Dao.RepertoireDAO;
import projet.data.Repertoire;

public class CreateFolder implements Commande {

	private static final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	private String nomRepertoire;
	private static final RepertoireDAO repDAO = DAOFactory.getRepertoireDAO();
	 
	public CreateFolder(String nom) {
		nomRepertoire = nom;
	}
	
	public void execute() 
	{
		Date date = new Date(0);

		if (!repDAO.existe("INBOX")) {
			Repertoire INBOX = new Repertoire("INBOX", null, System.getProperty("user.name").toString(), dateFormat.format(date).toString());
			repDAO.create(INBOX);
		}
		
	
		String[] reps = Repertoire.getActuel().split("/");
		Repertoire rep = new Repertoire(nomRepertoire, reps[reps.length-1], System.getProperty("user.name").toString(), dateFormat.format(date).toString());

		if (!repDAO.existe(nomRepertoire))
			repDAO.create(rep);
		else
			System.err.println(ansi().fgBrightRed().a("\n----- Erreur ----- : un répertoire nommé \""+nomRepertoire+"\" existe déja.\n").reset());
			
	}
}
