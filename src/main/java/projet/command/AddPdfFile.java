package projet.command;

import java.nio.file.Path;
import java.util.ArrayList;

import projet.Dao.DAO;
import projet.Dao.DAOFactory;
import projet.data.Fichier;
import projet.data.FichierFactory;

public class AddPdfFile implements Commande {

	private String cheminFichier;
	private static final DAO<Fichier> ficDAO = DAOFactory.getFichierDAO();;
	 
	public AddPdfFile(String chemin) {
		cheminFichier = chemin;
	}
	
	
	public void execute() 
	{
		ArrayList<Fichier> fichiers = new ArrayList<Fichier>();
		
		//Upload
		ArrayList<Path> paths = Fichier.upload(cheminFichier);
		//Création d'objets
		for (Path path : paths)
			fichiers.add(FichierFactory.getFichier(path));
		//Ajout à la BD
		for (Fichier fichier : fichiers)
			ficDAO.create(fichier);
			
	}

}
