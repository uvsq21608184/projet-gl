package projet.command;

import projet.Dao.DAOFactory;
import projet.Dao.FichierDAO;
import projet.data.Fichier;
import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;
public class DeleteFileText implements Commande {

	private String nomFichier;
	private static final FichierDAO ficDAO = DAOFactory.getFichierDAO();
	 
	public DeleteFileText(String nom) {
		nomFichier = nom;
	}
	
	public void execute() 
	{
		if (Fichier.existeINBOX(nomFichier))
		
			Fichier.deleteINBOX(nomFichier);
		if (ficDAO.existe(nomFichier))
	
			ficDAO.delete(nomFichier);
		else
			System.err.println(ansi().fgBrightRed().a("\n----- Erreur ----- : le fichier \""+nomFichier+"\" n'existe pas.\n").reset());
			
	}
}
